<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Home](./index.md) &gt; [calckey-js](./calckey-js.md) &gt; [entities](./calckey-js.entities.md) &gt; [FollowingFollowerPopulated](./calckey-js.entities.followingfollowerpopulated.md)

## entities.FollowingFollowerPopulated type

**Signature:**

```typescript
export declare type FollowingFollowerPopulated = Following & {
	follower: UserDetailed;
};
```
**References:** [Following](./calckey-js.entities.following.md)<!-- -->, [UserDetailed](./calckey-js.entities.userdetailed.md)


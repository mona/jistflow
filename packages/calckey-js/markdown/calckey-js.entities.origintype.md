<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Home](./index.md) &gt; [calckey-js](./calckey-js.md) &gt; [entities](./calckey-js.entities.md) &gt; [OriginType](./calckey-js.entities.origintype.md)

## entities.OriginType type

**Signature:**

```typescript
export declare type OriginType = "combined" | "local" | "remote";
```

<!-- Do not edit this file. It is automatically generated by API Documenter. -->

[Home](./index.md) &gt; [calckey-js](./calckey-js.md) &gt; [entities](./calckey-js.entities.md) &gt; [FollowRequest](./calckey-js.entities.followrequest.md)

## entities.FollowRequest type

**Signature:**

```typescript
export declare type FollowRequest = {
	id: ID;
	follower: User;
	followee: User;
};
```
**References:** [ID](./calckey-js.entities.id.md)<!-- -->, [User](./calckey-js.entities.user.md)


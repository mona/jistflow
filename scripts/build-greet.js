const chalk = require("chalk");

const themeColor = chalk.hex("#31748f");
console.log(themeColor("   ___      _      _              "));
console.log(themeColor("  / __\\__ _| | ___| | _____ _   _ "));
console.log(themeColor(" / /  / _` | |/ __| |/ / _  | | |"));
console.log(themeColor("/ /__| (_| | | (__|   <  __/ |_| |"));
console.log(themeColor("\\____/\\__,_|_|\\___|_|\\_\\___|\\__, |"));
console.log(themeColor("                            (___/ "));

console.log(" Currently building Calckey!");
console.log(
	chalk.rgb(255, 136, 0)(" Hang on for a moment, as this may take a while."),
);
console.log("");
